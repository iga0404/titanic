import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    titles = ['Mr.', 'Mrs.', 'Miss.']
    filtered_data = df[df['Name'].str.contains('|'.join(titles), na=False)]

    median_age_by_title = filtered_data.groupby('Name')['Age'].median()

    df['Age'].fillna(-1, inplace=True)

    df['Age'] = df.apply(
        lambda row: median_age_by_title.get(row['Name'], row['Age']) if row['Age'] == -1 else row['Age'], axis=1)

    missing_values_by_title = filtered_data.groupby('Name')['Age'].apply(lambda x: x.isna().sum())

    result = [(title, missing_values_by_title.get(title, 0), round(median_age_by_title.get(title, -1))) for title in
              titles]

    return result
